#!/bin/bash

# ============================================================================ #
# Grupo: uno
#
# Autores:
#         Alejandro Pereyra
#         Gabriel Hruza
#         Juan Godoy
#         Sebastian Scatularo
# ============================================================================ #

FILE=prueba.txt
TIME=30s

# Obtenemos el estado del proeceso y verificamos si esta en el grupo de 
# procesos que se encuentran corriendo en primer plano Si es asi, se interrumpe 
# la ejecucion informando que el script se debe lanzar en segundo plano. Si no 
# se continua con la ejecucion

if [[ $(ps -o stat -p $$) == *+* ]] 
then 
	echo "Debe ejecutar el script en segundo plano $0 &"
	exit
else
	# En un bucle infinito comprobamos si existe el archivo
	# si no existe informamos, si existe continuamos comprobando
	while(true)
	do
		if [ ! -f "$FILE" ]
		then
			echo "No existe el fichero $FILE \n"
		fi
		sleep $TIME
	done
fi
