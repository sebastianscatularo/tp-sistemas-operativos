#!/bin/bash

# ============================================================================ #
# Grupo: uno
#
# Autores:
#         Alejandro Pereyra
#         Gabriel Hruza
#         Juan Godoy
#         Sebastian Scatularo
# ============================================================================ #

DESTINO="backupC/$(date +%Y%m%d)"

if ! [ -d $DESTINO ]
then 
	echo "No existe $DESTINO, creando ..."
	mkdir -p $DESTINO
else
	echo "Existe $DESTINO, reusando ..."
fi

echo "Copiando archivos desde $PWD a $DESTINO"
cp "$PWD/"*.{dat,c} "$DESTINO"
