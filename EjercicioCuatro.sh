#!/bin/bash

# ============================================================================ #
# Grupo: uno
#
# Autores:
#         Alejandro Pereyra
#         Gabriel Hruza
#         Juan Godoy
#         Sebastian Scatularo
# ============================================================================ #

if ! [ $# -eq 1 ]; then
        echo "Debe ingresar el directorio destino." 
        echo "$0 <destino>"
        echo "destino: Directorio donde se va a copiar los archivos .dat y .c"
        exit 1
fi

DESTINO=$1

if ! [ -d $DESTINO ]
then 
	echo "No existe $DESTINO, creando ..."
	mkdir $DESTINO
else
	echo "Existe $DESTINO, reusando ..."
fi

echo "Copiando archivos desde $PWD a $DESTINO"
cp "$PWD/"*.{dat,c} "$DESTINO"
