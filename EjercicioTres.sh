#!/bin/bash

# ============================================================================ #
# Grupo: uno
#
# Autores:
#         Alejandro Pereyra
#         Gabriel Hruza
#         Juan Godoy
#         Sebastian Scatularo
# ============================================================================ #

if ! [ $# -eq 1 ]
then
	echo "Debe ingresar el directorio a listar" 
        echo "$0 <destino>"
        echo "destino: Directorio a listar recursivamente"
        exit 1
fi

DESTINO=$1

# Comprobamos si es un directorio, si no lo es informamos del error
# si no listamos recursivamente
if ! [ -d $DESTINO ]
then
	echo "Error: el destino ingresado no es un directorio"
else
	ls -R $DESTINO
fi
