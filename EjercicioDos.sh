#!/bin/bash

# ============================================================================ #
# Grupo: uno
#
# Autores:
#         Alejandro Pereyra
#         Gabriel Hruza
#         Juan Godoy
#         Sebastian Scatularo
# ============================================================================ #

DESTINO=$1

if ! [ $# -eq 1 ]; then
	echo "Debe ingresar el directorio destino." 
	echo "$0 <destino>"
	echo "destino: Directorio donde se va a almacenar el listado comprimido"
	exit 1
fi

# Creamos un directorio temporal para almacenar el estado intermedio
# de procesamiento
TMP=$(mktemp -d)
# Generamos el listado del home del usuario actual
echo "Generando listado ..."
ls -R $HOME > $TMP/listado 2> /dev/null
# Comprimimos el listado
echo "Comprimiendo listado ..."
tar -cvzf $TMP/listado.tar.gz -C $TMP listado > /dev/null 2> /dev/null
# Copiamos el listado a la carpeta destino
echo "Copiando listado a $DESTINO ..."
cp $TMP/listado.tar.gz $DESTINO
# Removemos la carpeta temporal
echo "Limpiando archivos temporales ..."
rm -rf $TMP
